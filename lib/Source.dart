import 'dart:convert';

class Source{


  dynamic id;
  dynamic name;

  Source({required this.id, required this.name});
  factory Source.fromJson(Map<String, dynamic> json){

return Source(
    id : json['id']??'',
    name: json['name']??'');
  }
}