
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter_news/Article_Model.dart';
import 'package:http/http.dart';


class API_Service {
  final ulr =
      'https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=beb7530af0a147ba88326ca22b662805';

  Future<List<Article_Model>> getNews() async {
    Response response = await get(Uri.parse(ulr));

    // Use the compute function to run parsePhotos in a separate isolate.

    if (response.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      List<dynamic> body = json['articles'];
      List<Article_Model> article =
          body.map((dynamic item) => Article_Model.fromJson(item)).toList();

      return article;
    } else {
      throw ('cant load data');
    }
  }
}
