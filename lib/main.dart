import 'package:flutter/material.dart';
import 'package:flutter_news/API_Service.dart';
import 'package:flutter_news/Article_Model.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
   MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  API_Service client = API_Service();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: FutureBuilder(
          future: client.getNews(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Article_Model>> snapshot) {
            print('object');
            print(snapshot);
            if (snapshot.hasData) {
              List<Article_Model>? articledata = snapshot.data;
              print(articledata?.length);


              return  Center(
                child: ListView.builder(
                    itemCount: articledata?.length,
                    itemBuilder: (context, index) {
                      print("index");
                      print(index);
                 return ListTile(
                   onTap: (){

                   },
                   leading: Image.network(articledata?[index].urlToimg ??''),
                   title: Text(articledata?[index].title),
                    //title: Text(articledata != null? [index].title:''),
                  );

                },
                ),
              );
            }else{
              print("not found");
            }
            return  Center(
              child: Text("Loading.....")
            );
          },
        ));
  }
}
