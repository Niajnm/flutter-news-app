import 'package:flutter_news/Source.dart';

class Article_Model {
 Source source;
  var author;
  var title;
  var description;
  var url;
  var urlToimg;
  var publishedAt;
  var content;

  Article_Model({required this.source, required this.author,required this.title, required this.description,
    required this.url, required this.urlToimg, required this.publishedAt, required this.content});

  factory Article_Model.fromJson(Map<dynamic, dynamic> json) {
    return Article_Model(
        source: Source.fromJson(json['source']??''),
        author: json['author']??'',
        title: json['title']??'',
        description: json['description']??'',
        url: json['url'] ?? '',
        urlToimg: json['urlToImage']??'',
        publishedAt: json['publishedAt']??'',
        content: json['content']??'');
  }
}
